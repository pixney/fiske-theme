<?php

return [
    'sidebar_hover' => 'anomaly.field_type.boolean',
    'navigation'    => [
        'type'       => 'anomaly.field_type.text',
        'input_view' => 'pixney.theme.fiske::admin/navigation/preferences',
    ],
];
