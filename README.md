# Fiske
Fiske Theme is a Pyro Development Platform control panel theme based on the excellent default Accelerant theme

We are only making very minor changes to the html structure to make sure updating won't be too time consuming for us. It's mostly css changes we are making - for now!

![Accelerante Plus](https://cdn.pbrd.co/images/GG7PL8f.png)

## Installing
Require `"pixney/fiske-theme":"dev-master"`  
Add to repositories:
```
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/pixney/fiske-theme"
    }
 ]
```

## Roadmap


## Change log
**2017-08-20**  
Big changes since we decided to refactor some html stucture and remove all old css files (almost) and do it all
from scratch instead of overwriting the Accelerant files. It quickly became messy. So a lot of bugs to be expected and
back to no mobile support at all.

With the refactor process, we also updated bootstrap to the newest beta version of 4.0.